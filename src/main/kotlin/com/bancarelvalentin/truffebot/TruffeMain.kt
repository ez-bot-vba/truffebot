package com.bancarelvalentin.truffebot

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcLiveConfig

// https://discord.com/oauth2/authorize?client_id=904051362533355582&scope=bot&permissions=8
class TruffeMain {
    
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            ConfigHandler.config = TruffeCommonConfig()
            ConfigHandler.liveConfig = GlrcLiveConfig()
            CommonUtils.addAllProcessToOrchestrator()
            Orchestrator.start()
        }
    }
}